var contestants = [];
var socket = null;

function save() {
  var savefile = [];
  contestants.forEach(function(contestant, idx) {
    var imgsrc = contestant.elm.querySelector("img").src;
    imgsrc = "portrait" + imgsrc.substring(imgsrc.lastIndexOf("/"));
    savefile.push({
      id: contestant.elm.id,
      img: imgsrc,
      score: contestant.score
    });
  });
  localStorage.setItem("taskmastersave", JSON.stringify(savefile));
}

function update() {
  // update scores
  contestants.forEach(function(contestant) {
    contestant.score += parseInt(contestant.award.value || 0);
    contestant.award.value = 0;
    contestant.points.textContent = contestant.score;
  });
  // sort
  contestants.sort(function(a, b) {
    return a.score - b.score;
  });
  var between = 100 / (contestants.length + 1);
  contestants.forEach(function(contestant, idx) {
    contestant.elm.style.left = [
      //idx * 18 + 14, "%" //works for 5
      (idx * between + between) + "%"
    ].join("");
    // winner(s)
    contestant.elm.classList.remove("winner");
    if(contestant.score &&
       contestant.score == contestants[contestants.length - 1].score) {
      contestant.elm.classList.add("winner");
    }
    var imgsrc = contestant.elm.querySelector("img").src;
    imgsrc = "portrait" + imgsrc.substring(imgsrc.lastIndexOf("/"));
  });
  save();
}

function song(audio) {
  if(audio.currentTime && audio.currentTime != audio.duration) {
    var fadeAudio = setInterval(function() {
      if(audio.volume > 0.1) {
        audio.volume -= 0.1;
      } else {
        clearInterval(fadeAudio);
        audio.pause();
        audio.currentTime = 0;
      }
    }, 100);
  } else {
    audio.volume = 1;
    audio.currentTime = 0;
    audio.play();
  }
}

function black() {
  var elm = document.querySelector("#black");
  var sho = document.querySelector("#showoff");
  if(!elm.classList.toggle("solid")) {
    sho.classList.remove("solid");
  } else {
    setTimeout(function() {
      sho.style.backgroundImage = "url('res/black.png')";
      //console.log("blacked");
    }, 500);
  }
}
function showoff(src) {
  var elm = document.querySelector("#showoff");
  var blk = document.querySelector("#black");
  //console.log(src, elm.style.backgroundImage, elm.style.backgroundImage.indexOf(src));
  elm.style.backgroundImage = "url(" + src + ")";
  if(!elm.classList.contains("solid")) {
    elm.classList.add("solid");
  }
  if(blk.classList.contains("solid")) {
    blk.classList.remove("solid");
  }
}

function loadportraits() {
  if(location && location.protocol == "file:") {
    console.error("run from a webserver (not file:///) to allow swapping images");
    return;
  }
  var sel = document.querySelector("select.portrait");
  var imgs = {};  // { "autumn": "portrait/autumn.png", ... }
  var r = new XMLHttpRequest();
  r.open("GET", "portrait/", true);
  r.onreadystatechange = function () {
    if (r.readyState != 4 || r.status != 200) {
      return;
    }
    //console.log(r.responseText);
    //alert("Success: " + r.responseText);
    var clean = "";
    var parser = new DOMParser();
    var doc = parser.parseFromString(r.responseText, "text/html");
    doc.querySelectorAll("a").forEach(function(link) {
      if(link.href.indexOf(".png") > 0) {
        clean = link.href.substring(link.href.lastIndexOf("/") + 1);
        imgs[clean] = "portrait/" + clean;
      }
      //console.log(link.href);
      while(sel.firstChild) {
        sel.removeChild(sel.firstChild);
      }
      for(img in imgs) {
        var opt = document.createElement("option");
        opt.textContent = img.substring(0, img.lastIndexOf(".")).replace(/^\w/, (c) => c.toUpperCase());
        opt.value = imgs[img];
        sel.appendChild(opt);
      }
    });
  };
  r.send();
}

function reset() {
  contestants.forEach(function(contestant) {
	contestant.score = contestant.award.value = 0;
	contestant.points.textContent = contestant.score;
  });
  update();
  document.body.classList.remove("inputmode");
}


window.addEventListener("load", function() {
  var elms = document.querySelectorAll(".contestant");
  var picker = document.querySelector("select.portrait");
  elms.forEach(function(elm, idx) {
	var sel = elm.querySelector("select");
	var pts = elm.querySelector(".points");
    var img = elm.querySelector("img");
    contestants.push({
      elm: elm,
      points: pts,
      award: sel,
      score: 0,
      slot: idx
    });
    elm.style.left = [
      idx * 18 + 14, "%"
    ].join("");
	var opt = null;
	for(var i = -5; i <= 5; ++i) {
	  opt = document.createElement("option");
	  opt.value = opt.textContent = i;
	  if(i >= 0) {
		opt.textContent = "+" + opt.textContent;
	  }
	  opt.selected = !i;
	  sel.appendChild(opt);
	}
    sel.addEventListener("click", function(e) {
      //e.preventDefault();
      e.stopPropagation();
    });
    img.addEventListener("click", function(e) {
      if(!document.querySelector("select.portrait option")) {
        console.log("dynamic portrait loading is unavailable.");
        return;
      }
      e.preventDefault();
      e.stopPropagation();
      picker.value = "portrait" + img.src.substring(img.src.lastIndexOf("/"));
      if(img.previousSibling === picker) {
        document.querySelector(".hidden").appendChild(picker);
      } else {
        elm.insertBefore(picker, img);
      }
    });
	pts.addEventListener("click", function(e) {
      e.preventDefault();
      e.stopPropagation();
	  if(!document.body.classList.contains("inputmode")) {
		document.body.classList.toggle("inputmode");
	  }
	});
  });

  //update();
  loadportraits();

  var savefile = localStorage.getItem("taskmastersave");
  var cont = [];  // [ {"img": "", "score": 0}, ... ]
  if(savefile) {
	try {
      cont = JSON.parse(savefile);
      //console.log("loaded", cont);
      cont.forEach(function(c) {
        //console.log(contestants[idx], c.img, c.score);
        var idx = contestants.findIndex(function(item) {
          return item.elm.id === c.id;
        });
        contestants[idx].elm.querySelector("img").src = c.img;
        contestants[idx].points.textContent = c.score;
        contestants[idx].score = c.score;
      });
	} catch(ignore) {
    }
  }
  update();

  window.addEventListener("click", function(e) {
//	if(!document.body.classList.contains("inputmode")) {
//    if(document.querySelector("select.portrait") != e.target) {
console.log(e.target);
    document.body.classList.toggle("inputmode");
//    }
    document.querySelector(".hidden").appendChild(document.querySelector("select.portrait"));
  });

  document.querySelector("#add").addEventListener("click", function(e) {
    e.preventDefault();
    e.stopPropagation();
    document.querySelector(".hidden").appendChild(document.querySelector("select.portrait"));
    document.body.classList.toggle("inputmode");
    update();
  });

  document.querySelector("#clear").addEventListener("click", function(e) {
    e.preventDefault();
    e.stopPropagation();
	if(!document.body.classList.contains("inputmode")) {
	  return;
	}
    if(!confirm("Reset all scores to zero?")) {
	  return;
    }
    reset();
  });

  document.querySelector("select.portrait").addEventListener("click", function(e) {
//    e.preventDefault();
    e.stopPropagation();
  });
  document.querySelector("select.portrait").addEventListener("change", function(e) {
    e.preventDefault();
    e.stopPropagation();
    this.nextSibling.src = this.value;
    document.querySelector(".hidden").appendChild(e.target);
    save();
  });

  var songlink = document.querySelector("#song");
  if(songlink) {
    songlink.addEventListener("click", function(e) {
      e.preventDefault();
      song(document.querySelector("#music"));
    });
  }

  var full = false;
  var fulllink = document.querySelector("#fullscreen");
  if(fulllink) {
      fulllink.addEventListener("click", function(e) {
        e.preventDefault();
        if(!full) {
          document.documentElement.requestFullscreen();
        } else {
          document.exitFullscreen();
        }
        full = !full;
      });
  }

  if(typeof io != "undefined") {  // allow serverless
    socket = io();
    socket.on("cmd", function(cmd) {
      console.log("got cmd", cmd);
      if(cmd.music) {
        if(cmd.mini) {
          song(document.querySelector("#musicmini"));
        } else if(cmd.task) {
          song(document.querySelector("#musictask"));
        } else {
          song(document.querySelector("#music"));
        }
      }
      if(cmd.award) {
        for(id in cmd.award) {
          document.querySelector("#" + id + " select").value = cmd.award[id];
        }
        update();
      }
      if(cmd.black) {
        black();
      }
      if(cmd.showoff) {
        showoff(cmd.src);
      }
      if(cmd.reset) {
        reset();
      }
    });
  }
});
