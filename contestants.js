/*
  Here's where you list your contestants!
  These names refer to PNG image files in the "portrait" folder:
  portrait/me.png  =  "me"

  - Replace the default example names with your own.
  - The list may have anywhere from two to ten items in it.
 */
var CONTESTANTS = [
  "chico", "harpo", "groucho", "stan", "oliver"
];
