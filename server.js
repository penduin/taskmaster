const express = require('express');
const app = express();
const http = require('http');
const fs = require('fs');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});
app.get('/control', (req, res) => {
  res.sendFile(__dirname + '/control.html');
});
app.get('/portrait/', (req, res) => {
  fs.readdir('portrait/', (err, files) => {
    res.write('<html><body>');
    files.forEach(file => {
      res.write('<a href="' + file + '">' + file + '</a><br>');
    });
    res.write('</body></html>');
    res.end();
  });
});

app.get("*", function(req, res) {
  if(req.url.indexOf("/.") >= 0) {
    res.send("nope.");
  } else {
    //console.log(req.url);
    fs.stat(process.cwd() + req.url, (err, stat) => {
      if (!err) {
        res.sendFile(process.cwd() + req.url);
      } else {
        res.sendFile(__dirname + req.url);
      }
    });
  }
});

io.on('connection', (socket) => {
  console.log('a user connected');
  socket.on('cmd', (msg) => {
    console.log('cmd: ' + msg);
    socket.broadcast.emit('cmd', msg);
  });
});


server.listen(3000, () => {
  console.log('listening on *:3000');
});
